import { Component } from "react";

import InputMessage from "./input/InputMessage";
import OutputMessage from "./output/OutputMessage";

class ContentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messageInput : "Xin mời nhập message ...",
            messageOutput:[],
            likeImage:false
        }
    }

    changeInputMessageHandler = (value) => {
        console.log(value);
        this.setState({
            messageInput : value
        })
    }

    changeOutputMessageHandler = () => {
        this.setState({
            messageOutput:[...this.state.messageOutput, this.state.messageInput],
            likeImage:this.state.messageInput ? true : false
        })
    }
    render() {
        return (
            <div>
                <InputMessage messageInputProp = {this.state.messageInput} changeInputMessageProp = {this.changeInputMessageHandler} changeOutputMessageProp = {this.changeOutputMessageHandler} />
                <OutputMessage messageOutputProp = {this.state.messageOutput} likeImageProp = {this.state.likeImage} />
            </div>
        )
    }
}

export default ContentComponent;