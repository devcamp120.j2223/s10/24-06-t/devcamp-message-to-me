import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class OutputMessage extends Component {
    render() {
        return (
            <div>
                <div className="row mt-4">
                    <div className="col-12">
                    {
                        this.props.messageOutputProp.map((value, index) => {
                            return <p key={index}>{value}</p>
                        })
                    }
                    </div>
                </div>
                { 
                    this.props.likeImageProp ? 
                    <div className="row mt-2">
                        <div className="col-12">
                            <img src={likeImg} alt="like" width={100}/>
                        </div>
                    </div>  
                    : null
                }

            </div>
        )
    }
}

export default OutputMessage;